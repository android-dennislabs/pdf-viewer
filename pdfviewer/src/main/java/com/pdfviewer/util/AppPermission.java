package com.pdfviewer.util;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppPermission {

    public interface PermissionListener{
        void onPermissionGranted();
        void onPermissionDenied(List<String> deniedPermissions);
    }
    private AppCompatActivity activity;
    private String[] permissions;
    private final List<String> deniedMessage = new ArrayList<>();
    private PermissionListener permissionlistener;

    public AppPermission(Activity activity) {
        if(activity instanceof AppCompatActivity) {
            this.activity = (AppCompatActivity) activity;
        }
    }

    public AppPermission setPermissionListener(PermissionListener permissionlistener) {
        this.permissionlistener = permissionlistener;
        return this;
    }

    public AppPermission setDeniedMessage(String deniedMessage) {
        this.deniedMessage.add(deniedMessage);
        return this;
    }

    public AppPermission setPermissions(String... permissions) {
        this.permissions = permissions;
        return this;
    }

    public void check() {
        ActivityResultLauncher<String[]> requestPermissionLauncher =
                activity.registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
                    @Override
                    public void onActivityResult(Map<String, Boolean> result) {
                        boolean isGranted = true;
                        for (Map.Entry<String,Boolean> entry : result.entrySet()) {
                            if(!entry.getValue()){
                                isGranted = entry.getValue();
                            }
                        }
                        if(permissionlistener != null){
                            if(isGranted){
                                permissionlistener.onPermissionGranted();
                            }else {
                                permissionlistener.onPermissionDenied(deniedMessage);
                            }
                        }
                    }
                });

        if (checkSelfPermission()) {
            // You can use the API that requires the permission.
            if(permissionlistener != null){
                permissionlistener.onPermissionGranted();
            }
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            requestPermissionLauncher.launch(permissions);
        }
    }

    private boolean checkSelfPermission() {
        for (String permission : permissions){
            if(ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }

    public static AppPermission with(Activity activity) {
        return new AppPermission(activity);
    }

}
