package com.pdfviewer.util;

import android.app.Activity;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.reflect.TypeToken;
import com.helper.callback.Response;
import com.helper.task.TaskRunner;
import com.helper.util.BaseDynamicUrlCreator;
import com.helper.util.BaseUtil;
import com.helper.util.GsonParser;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.R;
import com.pdfviewer.model.PDFModel;


public class PDFDynamicShare {

    public static final String TYPE_PDF = "pdf";
    public static final String ACTION_TYPE = BaseDynamicUrlCreator.ACTION_TYPE;

    public static void share(Activity activity, View view, String title, PDFModel pdfModel, int currentPage, Response.Progress progress) {
        if(PDFViewer.getInstance().getDynamicShareListener() != null) {
            progress.onStartProgressBar();
            Screenshot.takeScreenShot(title, view, new TaskRunner.Callback<Uri>() {
                @Override
                public void onComplete(Uri imagePath) {
                    PDFViewer.getInstance().getDynamicShareListener().onSharePDF(activity, pdfModel, currentPage, imagePath, progress);
                }
            });
        }else {
            BaseUtil.showToast(activity, activity.getString(R.string.error_dynamic_share_message));
        }
    }

    public static boolean isValidPdfUrl(Uri url, String extraData) {
        return url != null && url.toString().contains(ACTION_TYPE) && url.getQueryParameter(ACTION_TYPE).equals(TYPE_PDF) && !TextUtils.isEmpty(extraData);
    }

    public static void open(Activity activity, Uri url, String extraData) {
        if(isValidPdfUrl(url, extraData)) {
            PDFModel pdfModel = GsonParser.fromJsonAll(extraData, new TypeToken<PDFModel>(){});
            PDFViewer.openPdfDownloadActivity(activity, pdfModel, false, false);
        }
    }
}