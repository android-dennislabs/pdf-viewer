package com.pdfviewer.util;

import android.app.Activity;
import android.net.Uri;
import android.view.View;

import com.config.statistics.model.StatsLevelsModel;
import com.helper.callback.Response;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.stats.PDFStatsModel;

public interface PDFCallback extends Response {

    interface Statistics {
        void onStatsUpdate(StatsLevelsModel<PDFStatsModel> response, String jsonData);
    }

    interface StatsListener {
        void onStatsUpdated();
    }

    interface LastUpdateListener {
        void onRecentPdfViewedUpdated();
    }

    interface BookmarkUpdateListener {
        void onBookmarkUpdate(int id, String bookmarkPages);
    }

    interface OnClickListenerWithDelete<T> {
        void onItemClicked(View view, T item);
        void onDeleteClicked(View view, T item);
        void onUpdateUI();
    }

    interface ActivityResultListener {
        void onActivityResult(int requestCode, int resultCode);
    }

    interface DynamicShare {
        void onSharePDF(Activity activity, PDFModel pdfModel, int currentPage, Uri imagePath, Response.Progress progress);
    }
}