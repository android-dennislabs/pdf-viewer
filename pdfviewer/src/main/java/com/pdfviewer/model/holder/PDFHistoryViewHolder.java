package com.pdfviewer.model.holder;

import android.content.Context;
import android.net.Uri;
import android.view.View;

import com.config.config.ConfigManager;
import com.google.gson.reflect.TypeToken;
import com.helper.callback.Response;
import com.helper.model.HistoryModelResponse;
import com.helper.model.base.BaseHistoryViewHolder;
import com.helper.util.LoggerCommon;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.R;
import com.pdfviewer.activity.PDFBookmarkActivity;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.task.InsertViewHistory;
import com.pdfviewer.task.TaskDeleteHistory;
import com.pdfviewer.task.UpdatePagePosition;
import com.pdfviewer.util.PDFCallback;
import com.pdfviewer.util.PDFFileUtil;

public class PDFHistoryViewHolder extends BaseHistoryViewHolder {

    private final boolean isEnableViewCount;

    public static int getLayout(){
        return R.layout.pdf_item_history;
    }

    public PDFHistoryViewHolder(View itemView, Response.OnListUpdateListener<HistoryModelResponse> onUpdateUiListener) {
        super(itemView, onUpdateUiListener);
        this.onUpdateUiListener = onUpdateUiListener;
        this.ivDelete.setVisibility(onUpdateUiListener == null ? View.GONE : View.VISIBLE);
        this.isEnableViewCount = ConfigManager.getInstance().isEnableStatistics();
    }

    public PDFHistoryViewHolder(View itemView, Response.OnListClickListener<HistoryModelResponse> onClickOverrideListener) {
        super(itemView, onClickOverrideListener);
        this.onClickOverrideListener = onClickOverrideListener;
        this.isEnableViewCount = (PDFViewer.getInstance().isEnableViewCount() || ConfigManager.getInstance().isEnableStatistics());
    }

    @Override
    public void initViews(View itemView) {

    }

    @Override
    public boolean isEnableViewCount() {
        return isEnableViewCount;
    }

    @Override
    public void onUpdateUi(HistoryModelResponse mItem) {

    }

    @Override
    public void onItemClicked(View view, int position, HistoryModelResponse item) {
        PDFModel pdfProperty = mItem.getJsonModel(new TypeToken<PDFModel>() {
        });
        if (PDFFileUtil.isDriveViewerAvailable(view.getContext())) {
            PDFFileUtil.openExternalViewerDrive(Uri.parse(pdfProperty.getFilePath()) , view.getContext());
            saveCurrentPosition(0 , pdfProperty , view.getContext());
        } else {
            PDFViewer.openPdfViewerFromHistory(view.getContext(), pdfProperty);
        }
        if (onUpdateUiListener != null) {
            onUpdateUiListener.onUpdateListItem(view, position, item);
        }
    }
    private void saveCurrentPosition(final int currentPage, PDFModel pdfModel , Context context) {
        if (pdfModel != null) {
            pdfModel.setOpenPagePosition(currentPage);
            new UpdatePagePosition(pdfModel, result -> {
                LoggerCommon.d("UpdatePagePosition", "Status:" + result);
                if (PDFViewer.getInstance().getRecentUpdateListener() != null) {
                    PDFViewer.getInstance().getRecentUpdateListener().onRecentPdfViewedUpdated();
                }
            }).execute(context);
        }
    }
    @Override
    public void onDeleteClicked(View view, int position, HistoryModelResponse item) {
        new TaskDeleteHistory(view.getContext(), mItem, new PDFCallback.Status<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                if (onUpdateUiListener != null) {
                    onUpdateUiListener.onRemovedListItem(view, position, item);
                }
            }
        }).execute();
    }

    public void setData(HistoryModelResponse mItem) {
        super.setData(mItem, "PDF", itemView.getContext().getString(R.string.pdf_views, mItem.getViewCountFormatted()));
    }
}