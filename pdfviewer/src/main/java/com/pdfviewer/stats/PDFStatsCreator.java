package com.pdfviewer.stats;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.config.config.ConfigManager;
import com.config.config.ConfigPreferences;
import com.config.statistics.interfaces.StatsContentMode;
import com.config.statistics.model.StatisticsModel;
import com.config.statistics.model.StatsLevelsModel;
import com.config.statistics.util.StatsJsonCreator;
import com.config.util.EncryptData;
import com.google.gson.reflect.TypeToken;
import com.helper.task.TaskRunner;
import com.helper.util.GsonParser;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.task.GetBookByIdTask;
import com.pdfviewer.util.PDFCallback;
import com.pdfviewer.util.PDFSupportPref;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class PDFStatsCreator {

    public static void saveStatsData(Context context, PDFModel clickedModel) {
        if(ConfigManager.getInstance().isEnableStatistics()) {
            String previousData = updatePreviousList(context, clickedModel);
            if(!TextUtils.isEmpty(previousData)) {
                PDFSupportPref.setPdfStatsData(context, previousData);
            }
        }
    }

    @Nullable
    public static List<PDFStatsModel> getPDFListData(Context context) {
        return GsonParser.fromJson(PDFSupportPref.getPdfStatsData(context), new TypeToken<List<PDFStatsModel>>() {});
    }

    @Nullable
    public static StatsLevelsModel<PDFStatsModel> getStatsResponseFromJson(String jsonData) {
        return GsonParser.fromJson(jsonData, new TypeToken<StatsLevelsModel<PDFStatsModel>>() {});
    }

    @MainThread
    public static void getStatsJsonData(Context context, PDFCallback.Statistics callback) {
        StatsLevelsModel<PDFStatsModel> model = new StatsLevelsModel<>();
        TaskRunner.getInstance().executeAsync(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return getFinalStatsData(context, model);
            }
        }, new TaskRunner.Callback<String>() {
            @Override
            public void onComplete(String result) {
                callback.onStatsUpdate(model, result);
            }
        });
    }

    @Nullable
    @WorkerThread
    public static String getStatsJsonData(Context context) {
        return getFinalStatsData(context, new StatsLevelsModel<>());
    }

    @Nullable
    @WorkerThread
    public static List<PDFStatsModel> getStatsListData(Context context) {
        StatsLevelsModel<PDFStatsModel> responseModel = new StatsLevelsModel<>();
        getFinalStatsData(context, responseModel);
        return responseModel.getLevels();
    }

    @MainThread
    public static boolean isDataAvailable(Context context) {
        return !TextUtils.isEmpty(getPDFStatsJsonData(context));
    }

    public static String getPDFStatsJsonData(Context context) {
        return PDFSupportPref.getPdfStatsData(context);
    }

    public static void clear(Context context) {
        if(ConfigPreferences.isEnableStatistics(context)) {
            PDFSupportPref.setPdfStatsData(context, "");
        }
    }

    private static String getFinalStatsData(Context context, StatsLevelsModel<PDFStatsModel> model) {
        List<PDFStatsModel> listOfIds = mergeWithDatabase(context, getPDFListData(context));
        if(listOfIds == null || listOfIds.size() == 0){
            return null;
        }
        model.setContentMode(StatsContentMode.OFFLINE);
        model.setLevels(listOfIds);
        return GsonParser.toJson(model, new TypeToken<StatsLevelsModel<PDFStatsModel>>() {});
    }

    private static List<PDFStatsModel> mergeWithDatabase(Context context, List<PDFStatsModel> listOfIds) {
        if(listOfIds == null || listOfIds.size() == 0){
            return null;
        }
        List<PDFStatsModel> finalList = new ArrayList<>();
        for(PDFStatsModel item : listOfIds){
            PDFModel pdfItem = GetBookByIdTask.fetchFromDB(context, item.getId(), item.getTitle());
            if(pdfItem != null && !TextUtils.isEmpty(pdfItem.getStatsJson())) {
                StatisticsModel statsModel = StatsJsonCreator.fromJson(EncryptData.decode(pdfItem.getStatsJson()));
                if (statsModel != null) {
                    item.setLevels(statsModel.getLevels());
                    finalList.add(item);
                }
            }
        }
        return finalList;
    }

    private static String updatePreviousList(Context context, PDFModel clickedModel) {
        List<PDFStatsModel> value = GsonParser.fromJson(PDFSupportPref.getPdfStatsData(context), new TypeToken<List<PDFStatsModel>>() {});
        if(value == null){
            value = new ArrayList<>();
        }
        boolean isItemNotExist = true ;
        for (PDFStatsModel item : value){
            if (item.getId() == clickedModel.getId() ){
                isItemNotExist = false ;
            }
        }
        if (isItemNotExist) {
            value.add(new PDFStatsModel(clickedModel.getId(), clickedModel.getTitle()));
        }
        if(value.size() == 0){
            return "";
        }else {
            return GsonParser.toJson(value, new TypeToken<List<PDFModel>>() {});
        }
    }
}

