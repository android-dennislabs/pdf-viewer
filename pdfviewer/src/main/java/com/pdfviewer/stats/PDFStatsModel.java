package com.pdfviewer.stats;

import com.config.statistics.model.StatisticsLevel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by amit on 26/5/17.
 */

public class PDFStatsModel implements Serializable, Cloneable {

    public PDFStatsModel() {
    }

    public PDFStatsModel(int id, String title) {
        this.id = id;
        this.title = title;
    }

    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("levels")
    private ArrayList<StatisticsLevel> levels = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<StatisticsLevel> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<StatisticsLevel> levels) {
        this.levels = levels;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public PDFStatsModel getClone() {
        try {
            return (PDFStatsModel) clone();
        } catch (CloneNotSupportedException e) {
            return new PDFStatsModel();
        }
    }
}
