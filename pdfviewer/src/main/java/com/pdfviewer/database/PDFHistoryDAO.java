package com.pdfviewer.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.helper.model.HistoryModelResponse;
import com.pdfviewer.model.PDFHistoryModel;

import java.util.List;

@Dao
public interface PDFHistoryDAO {

    @Insert
    Long insertHistory(PDFHistoryModel record);

    @Query("SELECT * FROM pdf_history order by datetime(createdAt) DESC")
    List<HistoryModelResponse> getPDFHistory();

    @Query("SELECT *, COUNT(*) AS rowCount FROM (SELECT * FROM pdf_history ORDER BY createdAt DESC) AS x GROUP BY id ORDER BY createdAt DESC")
    List<HistoryModelResponse> getPDFHistoryUnique();

    @Query("DELETE FROM pdf_history WHERE id ==:id")
    void delete(int id);

    @Query("UPDATE pdf_history SET jsonData =:jsonData WHERE autoId ==:autoId")
    void updateMigrationJsonData(int autoId, String jsonData);

    @Query("DELETE FROM pdf_history")
    void clearAllRecords();

    @Query("SELECT COUNT(*) FROM pdf_history")
    int getPDFHistoryRowCount();

    @Query("SELECT MIN(autoId) FROM (Select autoId from pdf_history order by datetime(createdAt) DESC limit :maxCount)")
    int getMinRowCountWithLimit(int maxCount);

    @Query("DELETE FROM pdf_history WHERE autoId < :autoId")
    void clearAllRecordsLessThanAutoId(int autoId);
}
