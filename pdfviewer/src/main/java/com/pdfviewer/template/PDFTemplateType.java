package com.pdfviewer.template;

public interface PDFTemplateType {
    int DEFAULT = 0;
    int MONO = 1;
    int DI = 2;
    int TRI = 3;
    int QUARTZ = 4;
    int SELF_STUDY = 5;
}
