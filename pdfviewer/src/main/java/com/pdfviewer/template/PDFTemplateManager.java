package com.pdfviewer.template;

import android.content.Context;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.AnyRes;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public class PDFTemplateManager {

    private static PDFTemplateManager instance;

    private int templateType;

    public PDFTemplateManager setTemplateType(int templateType) {
        this.templateType = templateType;
        return this;
    }

    public int getTemplateType() {
        return templateType;
    }

    private PDFTemplateManager(int templateType) {
        this.templateType = templateType;
    }

    public static PDFTemplateManager get() {
        if (instance == null) {
            synchronized (PDFTemplateManager.class) {
                if (instance == null) instance = new PDFTemplateManager(PDFTemplateType.DEFAULT);
            }
        }
        return instance;
    }

    @AnyRes
    public int getResource(@NonNull PDFTemplateEntity entity) {
        switch (templateType) {
            case PDFTemplateType.MONO:
                return (entity.getMono() != PDFTemplateType.DEFAULT) ? entity.getMono() : entity.getDefault();
            case PDFTemplateType.DI:
                return (entity.getDi() != PDFTemplateType.DEFAULT) ? entity.getDi() : entity.getDefault();
            case PDFTemplateType.TRI:
                return (entity.getTri() != PDFTemplateType.DEFAULT) ? entity.getTri() : entity.getDefault();
            case PDFTemplateType.QUARTZ:
                return (entity.getQuartz() != PDFTemplateType.DEFAULT) ? entity.getQuartz() : entity.getDefault();
            case PDFTemplateType.SELF_STUDY:
                return (entity.getSelfStudy() != PDFTemplateType.DEFAULT) ? entity.getSelfStudy() : entity.getDefault();
            case PDFTemplateType.DEFAULT:
            default:
                return entity.getDefault();
        }
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    public void setColorFilter(@NonNull ImageView view, @NonNull PDFTemplateEntity entity) {
        @ColorInt int color = getColor(view.getContext(), entity);
        if (color != 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                view.setColorFilter(new BlendModeColorFilter(color, BlendMode.SRC_ATOP));
            } else {
                view.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    public void setColorFilterBackground(@NonNull View view, @NonNull PDFTemplateEntity entity) {
        @ColorInt int color = getColor(view.getContext(), entity);
        if (color != 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                view.getBackground().setColorFilter(new BlendModeColorFilter(color, BlendMode.SRC_ATOP));
            } else {
                view.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    public void setBackgroundResource(@NonNull View view, @NonNull PDFTemplateEntity entity) {
        @DrawableRes int drawable = getResource(entity);
        if (drawable != 0) {
            view.setBackgroundResource(drawable);
        }
    }

    public void setImageResource(@NonNull ImageView view, @NonNull PDFTemplateEntity entity) {
        @DrawableRes int drawable = getResource(entity);
        if (drawable != 0) {
            view.setImageResource(drawable);
        }
    }

    public void setBackgroundColor(@NonNull View view, @NonNull PDFTemplateEntity entity) {
        @ColorInt int color = getColor(view.getContext(), entity);
        if (color != 0) {
            view.setBackgroundColor(color);
        }
    }

    public void setTextColor(TextView view, @NonNull PDFTemplateEntity entity) {
        @ColorInt int color = getColor(view.getContext(), entity);
        if (color != 0) {
            view.setTextColor(color);
        }
    }

    @ColorInt
    private int getColor(Context context, PDFTemplateEntity entity) {
        return ContextCompat.getColor(context, getColor(entity));
    }

    @ColorRes
    public int getColor(PDFTemplateEntity entity) {
        return getResource(entity);
    }


    @DrawableRes
    public int getDrawable(PDFTemplateEntity entity) {
        return getResource(entity);
    }


    @LayoutRes
    public int getLayout(PDFTemplateEntity entity) {
        return getResource(entity);
    }


}
