package com.pdfviewer.template;

import androidx.annotation.AnyRes;

/**
 * Used this class for mapping template resources
 * like @DrawableRes, @ColorRes, @LayoutRes, @AttrRes, @StringRes, @DimenRes etc.
 */
public class PDFTemplateEntity {
    @AnyRes
    int Default;
    @AnyRes
    int Mono;
    @AnyRes
    int Di;
    @AnyRes
    int Tri;
    @AnyRes
    int Quartz;
    @AnyRes
    int SelfStudy;

    public static PDFTemplateEntity Builder() {
        return new PDFTemplateEntity();
    }

    public int getDefault() {
        return Default;
    }

    public PDFTemplateEntity setDefault(int aDefault) {
        this.Default = aDefault;
        return this;
    }

    public int getMono() {
        return Mono;
    }

    public PDFTemplateEntity setMono(int mono) {
        this.Mono = mono;
        return this;
    }

    public int getDi() {
        return Di;
    }

    public PDFTemplateEntity setDi(int di) {
        this.Di = di;
        return this;
    }

    public int getTri() {
        return Tri;
    }

    public PDFTemplateEntity setTri(int tri) {
        this.Tri = tri;
        return this;
    }

    public int getQuartz() {
        return Quartz;
    }

    public PDFTemplateEntity setQuartz(int quartz) {
        this.Quartz = quartz;
        return this;
    }

    public int getSelfStudy() {
        return SelfStudy;
    }

    public PDFTemplateEntity setSelfStudy(int selfStudy) {
        SelfStudy = selfStudy;
        return this;
    }
}
