package com.pdfviewer.template;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;

import androidx.annotation.ColorRes;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;

import com.helper.widget.ItemDecorationCardMargin;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.R;
import com.pdfviewer.util.PDFSupportPref;

public class PDFThemeManager {

    private static PDFThemeManager instance;

    private final PDFTemplateManager template;

    public static PDFThemeManager get(Context context) {
        if (instance == null) {
            synchronized (PDFThemeManager.class) {
                if (instance == null) instance = new PDFThemeManager(context);
            }
        }
        return instance;
    }

    private PDFThemeManager(Context context) {
        this.template = PDFTemplateManager.get().setTemplateType(PDFSupportPref.getTheme(context));
    }

    public void setTemplate(int templateType) {
        this.template.setTemplateType(templateType);
    }

    public static boolean isApplyCustomActionBar(Context context) {
        return PDFViewer.getInstance().getTemplate(context) != PDFTemplateType.DEFAULT
                && PDFViewer.getInstance().getTemplate(context) != PDFTemplateType.SELF_STUDY;
    }

    public static boolean isApplyTheme(Context context) {
        return PDFViewer.getInstance().getTemplate(context) != PDFTemplateType.DEFAULT
                && PDFViewer.getInstance().getTemplate(context) != PDFTemplateType.SELF_STUDY;
    }

    @LayoutRes
    public int getActivityFileDownload() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setSelfStudy(R.layout.pdf_main_downlaod_ss)
                .setDefault(R.layout.pdf_main_downlaod));
    }


//    public static int getToolbarTextColor(Activity activity) {
//        return Prefe;
//    }

    @LayoutRes
    public int getActivityBookmark() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setSelfStudy(R.layout.pdf_bookmark_activity)
                .setDefault(R.layout.pdf_bookmark_activity));
    }

    public int getCardBookmark() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(R.layout.pdf_item_bookmark)
                .setMono(R.layout.pdf_item_bookmark_theme1)
                .setDi(R.layout.pdf_item_bookmark_theme2)
                .setTri(R.layout.pdf_item_bookmark_theme3)
                .setQuartz(R.layout.pdf_item_bookmark_theme4)
                .setSelfStudy(R.layout.pdf_item_bookmark_theme_ss)
        );
    }

    public int getCardDownload() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(R.layout.pdf_item_download)
                .setMono(R.layout.pdf_item_bookmark_theme1)
                .setDi(R.layout.pdf_item_bookmark_theme2)
                .setTri(R.layout.pdf_item_bookmark_theme3)
                .setQuartz(R.layout.pdf_item_bookmark_theme4)
                .setSelfStudy(R.layout.pdf_item_download_theme_ss)
        );
    }

    public int getViewerMenu() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(R.menu.pdf_viewer_menu)
                .setMono(R.menu.pdf_viewer_menu_theme1)
                .setDi(R.menu.pdf_viewer_menu_theme2)
                .setTri(R.menu.pdf_viewer_menu_theme2)
                .setQuartz(R.menu.pdf_viewer_menu_theme2)
        );
    }

    public int getViewerScrollBar() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(R.drawable.default_scroll_handle_right)
                .setMono(R.drawable.pdf_scroll_handle_right_theme1)
                .setDi(R.drawable.pdf_scroll_handle_right_theme1)
                .setTri(R.drawable.pdf_scroll_handle_right_theme3)
                .setQuartz(R.drawable.pdf_scroll_handle_right_theme4)
        );
    }

    public int getViewerScrollBarTextColor() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(Color.BLACK)
                .setMono(Color.BLACK)
                .setDi(Color.BLACK)
                .setTri(Color.WHITE)
                .setQuartz(Color.WHITE)
        );
    }

    public int getMenuTextColor() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(R.array.colorsMenu)
        );
    }

    public int getDrawableToolbar() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(R.drawable.bg_action_bar_color_primary)
                .setMono(R.drawable.bg_action_bar_gradient)
                .setDi(R.drawable.bg_action_bar_color_primary)
                .setTri(R.drawable.bg_action_bar_color_primary)
                .setQuartz(R.drawable.bg_action_bar_color_primary)
        );
    }

    public int getToolbarTextColor() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setDefault(Color.WHITE)
                .setMono(Color.BLACK)
                .setDi(Color.WHITE)
                .setTri(Color.WHITE)
                .setQuartz(Color.WHITE)
        );
    }

    public int getSlotBookmarkText() {
        return template.getLayout(PDFTemplateEntity.Builder()
                .setMono(R.string.pdf_tag_bookmarked)
                .setDi(R.string.pdf_tag_bookmarked)
                .setTri(R.string.pdf_tag_bookmarked)
                .setQuartz(R.string.pdf_tag_bookmarked)
                .setDefault(R.string.pdf_tag_pages)
        );
    }

    public void addItemDecoration(RecyclerView recyclerView) {
        if (template.getTemplateType() == PDFTemplateType.SELF_STUDY) {
            if(recyclerView != null) recyclerView.addItemDecoration(new ItemDecorationCardMargin(recyclerView.getContext()));
        }
    }
}
