package com.pdfviewer.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import com.adssdk.AdsInterstitial;
import com.adssdk.AdsSDK;
import com.adssdk.util.AdsConstants;
import com.config.statistics.StatsManager;
import com.config.statistics.model.StatisticsModel;
import com.config.statistics.util.StatsJsonCreator;
import com.helper.callback.NetworkListener;
import com.helper.callback.Response;
import com.helper.util.BaseAnimationUtil;
import com.helper.util.BaseUtil;
import com.helper.util.EncryptUtil;
import com.helper.util.LoggerCommon;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.R;
import com.pdfviewer.analytics.BaseAnalyticsActivity;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.network.DownloadManager;
import com.pdfviewer.stats.PDFStatsCreator;
import com.pdfviewer.task.InsertViewHistory;
import com.pdfviewer.task.UpdatePagePosition;
import com.pdfviewer.template.PDFThemeManager;
import com.pdfviewer.util.PDFCallback;
import com.pdfviewer.util.PDFConstant;
import com.pdfviewer.util.PDFFileUtil;
import com.pdfviewer.util.PdfUtil;

import java.io.File;
import java.util.Locale;


public class PDFFileDownloadActivity extends BaseAnalyticsActivity implements DownloadManager.Progress {

    private String mFileUrl;
    private String mPdfFileName;
    private LinearLayout btnDownload;
    private LinearLayout ll_download;
    private TextView tv_download_percentage;
    private DownloadManager downloadManager;
    private View llDownloaderView;
    private String mPdfTitle;
    private boolean isAutoDownload = false;
    private PDFModel pdfModel;
    private boolean isOpenExternal = false;


//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(newBase);
//        SplitCompat.installActivity(this);
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(PDFThemeManager.get(this).getActivityFileDownload());
        downloadManager = new DownloadManager(this)
                .setProgressListener(this);
        PdfUtil.initBannerAd(this, findViewById(R.id.adViewtop));
        init();
        getDataFromIntent();
    }

    @Override
    public void onShowAdsInUi() {
//        RelativeLayout rlNativeAd = findViewById(R.id.full_ad);
//        PdfUtil.loadNativeAds(this, rlNativeAd, com.adssdk.R.layout.native_pager_ad_app_install, false);
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().getParcelable(PDFConstant.EXTRA_PROPERTY) instanceof PDFModel) {
            pdfModel = intent.getExtras().getParcelable(PDFConstant.EXTRA_PROPERTY);
            isAutoDownload = intent.getBooleanExtra(PDFConstant.IS_AUTO_DOWNLOAD, false);
            isOpenExternal = intent.getBooleanExtra(PDFConstant.IS_OPEN_EXTERNAL, false);
            if (pdfModel != null && !TextUtils.isEmpty(pdfModel.getTitle())
                    && !TextUtils.isEmpty(pdfModel.getPdfUrl())) {
                mPdfFileName = pdfModel.getPdf();//contains pdf file name.
                mPdfTitle = pdfModel.getTitle();//contains pdf title visible to user.
                mFileUrl = pdfModel.getPdfUrl();//contains .pdf extension
                downloadManager.setHostName(pdfModel.getHost());
                if(!TextUtils.isEmpty(pdfModel.getTitle()) && TextUtils.isEmpty(pdfModel.getStatsJson())) {
                    addStatisticsContent(getStatisticsLevel(pdfModel.getId(), pdfModel.getTitle()));
                }
                if (mPdfFileName == null) {
                    mPdfFileName = PDFFileUtil.getFileNameFromUrl(pdfModel.getPdfUrl());
                }
                if ( !TextUtils.isEmpty(pdfModel.getPdfBaseUrlPrefix()) ) {
                    downloadManager.setEndPoint( pdfModel.getPdfBaseUrlPrefix() );
                }
                downloadManager.loadFileIfExists(mPdfFileName);
            } else {
                PdfUtil.showToast(this, PDFConstant.INVALID_PROPERTY);
                finish();
            }
        } else {
            PdfUtil.showToast(this, PDFConstant.INVALID_PROPERTY);
            finish();
        }
    }


    void init() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Download Book");
        }

        llDownloaderView = findViewById(R.id.ll_downloader);
        btnDownload = findViewById(R.id.btnProgressBar);
        ll_download = findViewById(R.id.ll_download);
        tv_download_percentage = findViewById(R.id.tv_download_percentage);

        btnDownload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                downloadPdfFile(mFileUrl);
                getAppAnalytics().onPDFDownloadClick(mPdfTitle);
            }
        });

        PDFViewer.getInstance().addActivityResultCallbacks(this.hashCode(), new PDFCallback.ActivityResultListener() {
            @Override
            public void onActivityResult(int requestCode, int resultCode) {
                handleActivityResult(requestCode, resultCode);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PDFViewer.getInstance().removeActivityResultCallbacks(this.hashCode());
    }

    private String mStatistics;

    @Override
    protected void onStart() {
        super.onStart();
        mStatistics = TextUtils.isEmpty(pdfModel.getStatsJson()) ? getStatistics() : getUpdatedStatistics(pdfModel.getStatsJson());
    }

    private String getUpdatedStatistics(String statsJson) {
        String decodedJson = EncryptUtil.decode(statsJson);
        if(!TextUtils.isEmpty(decodedJson)) {
            StatisticsModel statsModel = StatsManager.parseStats(decodedJson);
            getSuperClass().addMoreDetails(statsModel);
            return EncryptUtil.encode(StatsJsonCreator.toJson(statsModel));
        }else {
            return null;
        }
    }

    private void downloadPdfFile(String pdfUrl) {
        if(mStatistics == null){
            mStatistics = TextUtils.isEmpty(pdfModel.getStatsJson()) ? getStatistics() : getUpdatedStatistics(pdfModel.getStatsJson());
        }
        llDownloaderView.setVisibility(View.VISIBLE);
        downloadManager.downloadFile(pdfUrl, mStatistics);
    }

    private void openPdfFromUriAds(Uri uri, Boolean isFileAlreadyDownloaded) {
        if ( AdsSDK.getInstance() != null
                && AdsSDK.isAdsEnable(this)
                && AdsSDK.getInstance().getAdsInterstitial() != null
                && AdsSDK.getInstance().getAdsInterstitial().isInterstitialAd() ) {
            AdsSDK.getInstance().getAdsInterstitial().showInterstitial(this, false , AdsConstants.CLICK, new AdsInterstitial.OnFullScreenAdsCallback() {
                @Override
                public void onAdLoaded() {

                }

                @Override
                public void onAdRequestFail() {
                    Log.e("AdActivity" , "InterstitialAd onAdRequestFail");
                    openPdfFromUri(uri, isFileAlreadyDownloaded);
                }

                @Override
                public void onAdClose() {
                    Log.e("AdActivity" , "InterstitialAd onAdClose");
                    openPdfFromUri(uri, isFileAlreadyDownloaded);
                }
            });
        }else {
            Log.e("AdActivity" , "InterstitialAd not found");
            openPdfFromUri(uri, isFileAlreadyDownloaded);
        }
    }
    private void openPdfFromUri(Uri uri, Boolean isFileAlreadyDownloaded) {
        if (!isOpenExternal) {
            if(mStatistics == null){
                mStatistics = TextUtils.isEmpty(pdfModel.getStatsJson()) ? getStatistics() : getUpdatedStatistics(pdfModel.getStatsJson());
            }
            if(isFileAlreadyDownloaded) {
                PDFStatsCreator.saveStatsData(this, pdfModel);
            }
            PDFViewer.openPdfViewerActivity(this, pdfModel.getClone(), pdfModel.getId(), mPdfTitle, mPdfFileName, pdfModel.getSubTitle(), mStatistics, uri, isFileAlreadyDownloaded, pdfModel.getViewCount());
        } else {
            if (PDFFileUtil.isDriveViewerAvailable(this)) {
                PDFFileUtil.openExternalViewerDrive(uri , this);
            } else {
                openExternalViewer(uri);
            }
            PDFModel updatePDFModel = updatePDFModel( pdfModel, pdfModel.getId(), mPdfTitle, mPdfFileName, pdfModel.getSubTitle(), mStatistics, uri, isFileAlreadyDownloaded, pdfModel.getViewCount());
            saveCurrentPosition(0 , updatePDFModel);
            finish();
        }
    }

    private PDFModel updatePDFModel(PDFModel model, int id, String pdfTitle, String pdfFileName, String subTitle, String statsJson, Uri fileUri, boolean isFileAlreadyDownloaded, int viewCount){
        PDFModel pdfModel = model.getClone();
        pdfModel.setId(id);
        pdfModel.setTitle(pdfTitle);
        pdfModel.setPdf(pdfFileName);
        pdfModel.setStatsJson(statsJson);
        pdfModel.setSubTitle(subTitle);
        pdfModel.setFilePath(fileUri.toString());
        pdfModel.setFileAlreadyDownloaded(isFileAlreadyDownloaded);
        pdfModel.setViewCount(viewCount);
        pdfModel.setViewCountFormatted(viewCount == 0 ? "" : BaseUtil.convertNumberUSFormat(viewCount));
        return pdfModel;
    }

    private void saveCurrentPosition(final int currentPage, PDFModel pdfModel) {
        if (pdfModel != null) {
            pdfModel.setOpenPagePosition(currentPage);
            new UpdatePagePosition(pdfModel, result -> {
                LoggerCommon.d("UpdatePagePosition", "Status:" + result);
                if (PDFViewer.getInstance().getRecentUpdateListener() != null) {
                    PDFViewer.getInstance().getRecentUpdateListener().onRecentPdfViewedUpdated();
                }
            }).execute(PDFFileDownloadActivity.this);

            new InsertViewHistory(pdfModel).execute(this);
        }
    }

    private void openExternalViewer(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/pdf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// don't use this in API30 and above
            DownloadManager.grantAllUriPermissions(this, intent, uri);
        }
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// don't use this in API30 and above
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isFirstHit = true;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        handleActivityResult(requestCode, resultCode);
    }

    private void handleActivityResult(int requestCode, int resultCode) {
        if (requestCode == PDFViewer.INTENT_PDF_LOAD_SUCCESS) {
            if (resultCode == RESULT_OK) {
                finish();
            } else {
                if (isFirstHit) {
                    isFirstHit = false;
                    if (!TextUtils.isEmpty(mFileUrl)) {
                        downloadPdfFile(mFileUrl);
                    }
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressManager(boolean isVisible) {
        if (isVisible) {
            btnDownload.setVisibility(View.GONE);
            ll_download.setVisibility(View.VISIBLE);
        } else {
            btnDownload.setVisibility(View.VISIBLE);
            ll_download.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDownloadedFileStatus(boolean isFileExists) {
        if (!isFileExists && isAutoDownload) {
            downloadPdfFile(mFileUrl);
        }
    }

    private final Handler handler = new Handler(Looper.myLooper());

    @Override
    public void onProgressUpdate(int progress) {
        if (handler != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("@Alpha", "Percentage:" + progress);
                    if (tv_download_percentage != null && progress >= 0) {
                        tv_download_percentage.setText(String.format(Locale.US, "%d %% ", progress));
                    }
                }
            });
        }
    }

    @Override
    public void onFileDownloaded(File file, Uri fileUri, String ext, String type, Boolean isFileAlreadyDownloaded) {
        llDownloaderView.setVisibility(View.GONE);
        openPdfFromUriAds(fileUri, isFileAlreadyDownloaded);
    }

    @Override
    public void onDownloadingError(Exception e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDownloadingCanceled() {

    }

    @Override
    public void onRetry(NetworkListener.Retry retryCallback) {
        PdfUtil.showNoDataRetry(llDownloaderView, retryCallback, new Response.Progress() {
            @Override
            public void onStartProgressBar() {
                showProgress(true);
            }

            @Override
            public void onStopProgressBar() {
                showProgress(false);
            }
        });
    }

    private void showProgress(boolean isVisible) {
        if (isVisible) {
            btnDownload.setVisibility(View.GONE);
            BaseAnimationUtil.alphaAnimation(ll_download, View.VISIBLE);
//            ll_download.setVisibility(View.VISIBLE);
        } else {
            btnDownload.setVisibility(View.GONE);
            ll_download.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (downloadManager != null) {
            downloadManager.cancelDownload();
        }
        super.onBackPressed();
    }
}
