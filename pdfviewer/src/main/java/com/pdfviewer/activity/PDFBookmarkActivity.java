package com.pdfviewer.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.adssdk.AdsInterstitial;
import com.adssdk.AdsSDK;
import com.adssdk.PageAdsAppCompactActivity;
import com.adssdk.util.AdsConstants;
import com.helper.task.TaskRunner;
import com.helper.util.LoggerCommon;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.R;
import com.pdfviewer.model.PDFBookmarkAdapter;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.stats.PDFStatsCreator;
import com.pdfviewer.task.InsertViewHistory;
import com.pdfviewer.task.UpdatePagePosition;
import com.pdfviewer.template.PDFThemeManager;
import com.pdfviewer.util.PDFCallback;
import com.pdfviewer.util.PDFConstant;
import com.pdfviewer.util.PDFFileUtil;
import com.pdfviewer.util.PdfUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


public class PDFBookmarkActivity extends PageAdsAppCompactActivity {

    private PDFBookmarkAdapter adapter;
    private final List<PDFModel> mList = new ArrayList<>();
    private boolean isShowDownloadedList = false;
    private View llNoData;


//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(newBase);
//        SplitCompat.installActivity(this);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(PDFThemeManager.get(this).getActivityBookmark());
        getDataFromIntent();

        initLayouts();

        if (getSupportActionBar() != null) {
            if(PDFThemeManager.isApplyCustomActionBar(this)){
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setCustomView(R.layout.pdf_action_bar);
                getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, PDFThemeManager.get(this).getDrawableToolbar()));
                View ivBack = findViewById(R.id.iv_back);
                TextView tvTitle = findViewById(R.id.tv_title);
                if(ivBack != null){
                    ivBack.setOnClickListener(v -> onBackPressed());
                }
                if(tvTitle != null){
                    tvTitle.setTextColor(PDFThemeManager.get(this).getToolbarTextColor());
                    String title = isShowDownloadedList ? getString(R.string.downloaded_books) : getString(R.string.pdf_bookmarked);
                    tvTitle.setText(title);
                }
            }else {
                String title = isShowDownloadedList ? getString(R.string.downloaded_books) : getString(R.string.pdf_bookmark);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setTitle(title);
                }
            }
        }
        PdfUtil.initBannerAd(this, findViewById(R.id.rl_ads));
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            isShowDownloadedList = intent.getBooleanExtra(PDFConstant.IS_SHOW_DOWNLOADED_LIST, false);
        }
    }

    private Handler handler;

    @Override
    protected void onResume() {
        super.onResume();
        if (handler == null) {
            handler = new Handler(Looper.myLooper());
        }
        fetchFromDatabase();
    }

    private final PDFCallback.LastUpdateListener recentUpdateListener = new PDFCallback.LastUpdateListener() {
        @Override
        public void onRecentPdfViewedUpdated() {
            fetchFromDatabase();
        }
    };

    private void initLayouts() {
        llNoData = findViewById(com.helper.R.id.ll_no_data);
        RecyclerView rvList = findViewById(R.id.rvList);
        PDFThemeManager.get(this).addItemDecoration(rvList);
        adapter = new PDFBookmarkAdapter(mList, isShowDownloadedList, new PDFCallback.OnListClickListener<PDFModel>() {
            @Override
            public void onItemClicked(View view, PDFModel item) {
                handlePDFOpenClickAds(item);
            }

            @Override
            public void onDeleteClicked(View view, int position, PDFModel item) {
                handleDeleteClick(view, position, item);
            }
        });
        rvList.setAdapter(adapter);

        PDFViewer.getInstance().addRecentUpdateListener(recentUpdateListener);
    }

    private void handlePDFOpenClickAds( PDFModel item) {
        if ( AdsSDK.getInstance() != null
                && AdsSDK.getInstance().getAdsInterstitial() != null
                && AdsSDK.getInstance().getAdsInterstitial().isInterstitialAd() ) {
            AdsSDK.getInstance().getAdsInterstitial().showInterstitial(this, false , AdsConstants.CLICK, new AdsInterstitial.OnFullScreenAdsCallback() {
                @Override
                public void onAdRequestFail() {
                    Log.e("AdActivity" , "InterstitialAd onAdRequestFail");
                    handlePDFOpenClick( item);
                }

                @Override
                public void onAdClose() {
                    Log.e("AdActivity" , "InterstitialAd onAdClose");
                    handlePDFOpenClick( item);
                }

                @Override
                public void onAdLoaded() {

                }
            });
        }else {
            Log.e("AdActivity" , "InterstitialAd not found");
            handlePDFOpenClick(item);
        }
    }
    private void handlePDFOpenClick( PDFModel item) {
        try {
            PDFStatsCreator.saveStatsData(PDFBookmarkActivity.this, item);
            if (PDFFileUtil.isDriveViewerAvailable(PDFBookmarkActivity.this)) {
                PDFFileUtil.openExternalViewerDrive(Uri.parse(item.getFilePath()) , PDFBookmarkActivity.this);
                saveCurrentPosition(0 , item);
            } else {
                PDFViewer.openPdfViewerActivity(PDFBookmarkActivity.this, item, true);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    private void saveCurrentPosition(final int currentPage, PDFModel pdfModel) {
        if (pdfModel != null) {
            pdfModel.setOpenPagePosition(currentPage);
            new UpdatePagePosition(pdfModel, result -> {
                LoggerCommon.d("UpdatePagePosition", "Status:" + result);
                if (PDFViewer.getInstance().getRecentUpdateListener() != null) {
                    PDFViewer.getInstance().getRecentUpdateListener().onRecentPdfViewedUpdated();
                }
            }).execute(PDFBookmarkActivity.this);
            new InsertViewHistory(pdfModel).execute(this);
        }
    }

    private void fetchFromDatabase() {
        TaskRunner.getInstance().executeAsync(new Callable<List<PDFModel>>() {
            @Override
            public List<PDFModel> call() throws Exception {
                List<PDFModel> finalList = new ArrayList<>();
                List<PDFModel> mLocalList;
                if (isShowDownloadedList) {
                    mLocalList = PDFViewer.getInstance().getDatabase(PDFBookmarkActivity.this).pdfViewerDAO().fetchAllDownloadedData();
                } else {
                    mLocalList = PDFViewer.getInstance().getDatabase(PDFBookmarkActivity.this).pdfViewerDAO().fetchAllData();
                }
                List<String> mStorageFileList = PDFFileUtil.getStorageFileList(PDFBookmarkActivity.this);
                if(mStorageFileList != null && mLocalList != null && mLocalList.size() > 0) {
                    for (PDFModel item : mLocalList) {
                        String fileName = getFileName(item.getFilePath());
                        if (mStorageFileList.contains(fileName)) {
                            finalList.add(item);
                        }
                    }
                }
                return finalList;
            }
        }, new TaskRunner.Callback<List<PDFModel>>() {
            @Override
            public void onComplete(List<PDFModel> result) {
                if (result != null) {
                    loadList(result);
                } else {
                    loadList(null);
                }
            }
        });
    }

    private String getFileName(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return "";
        }
        if (!filePath.contains("/")) {
            return filePath;
        }
        return filePath.substring(filePath.lastIndexOf("/") + 1);
    }

    private void loadList(List<PDFModel> list) {
        PdfUtil.showNoData(llNoData, View.GONE);
        mList.clear();
        if (list != null && list.size() > 0) {
            mList.addAll(list);
        } else {
            PdfUtil.showNoData(llNoData, View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void handleDeleteClick(View v, int position, PDFModel model) {
        if (model != null) {
            TaskRunner.getInstance().executeAsync(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    if (isShowDownloadedList) {
                        PDFViewer.getInstance().getDatabase(v.getContext()).pdfViewerDAO().delete(model.getId(), model.getTitle());
                        PDFFileUtil.deleteFile(PDFBookmarkActivity.this, model.getPdf());
                    } else {
                        PDFViewer.getInstance().getDatabase(v.getContext()).pdfViewerDAO().updateBookmarkPages(model.getId(), model.getTitle(), "", PdfUtil.getDatabaseDateTime());
                    }
                    return true;
                }
            }, new TaskRunner.Callback<Boolean>() {
                @Override
                public void onComplete(Boolean result) {
                    try {
                        if(mList != null) {
                            if (mList.size() > 0 && mList.size() > position) {
                                mList.remove(position);
                                adapter.notifyItemRemoved(position);
                                adapter.notifyItemRangeChanged(position, adapter.getItemCount());
                            }
                            if (mList.size() == 0) {
                                PdfUtil.showNoData(llNoData, View.VISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}