package com.pdfviewer.analytics;

import android.content.Context;
import android.os.Bundle;

public class AppAnalytics extends AnalyticsUtil {

    private static volatile AppAnalytics sSoleInstance;

    private AppAnalytics(Context context) {
        super(context);
    }

    public static AppAnalytics getInstance(Context context) {
        if (sSoleInstance == null) {
            synchronized (AppAnalytics.class) {
                if (sSoleInstance == null) sSoleInstance = new AppAnalytics(context);
            }
        }
        return sSoleInstance;
    }

    public void onHomePageDrawerClick(String itemName) {
        setUserProperty(AnalyticsKeys.HOME_PAGE_DRAWER_CLICK, itemName);
    }

    public void onHomePageTabsClick(@AnalyticsKeys.TabType String tabName) {
        setUserProperty(AnalyticsKeys.HOME_PAGE_TABS_CLICK, tabName);
    }

    public void onHomePageMenuClick(String itemName) {
        setUserProperty(AnalyticsKeys.HOME_PAGE_MENU_CLICK, itemName);
    }

    public void onHomePagePositionChangeClick(String itemName) {
        setUserProperty(AnalyticsKeys.HOME_PAGE_POSITION_CHANGE_CLICK, itemName);
    }

    //Log Events
    public void onUpdatePageArticleClick(String articleId, String articleTitle) {
        Bundle params = new Bundle();
        if (articleId != null) {
            params.putString(AnalyticsKeys.Param.ARTICLE_ID, articleId);
        }
        if (articleTitle != null) {
            params.putString(AnalyticsKeys.Param.ARTICLE_TITLE, articleTitle);
        }
        sendEvent(AnalyticsKeys.UPDATE_PAGE_ARTICLE_CLICK, params);
    }

    public void onHomePageCategoryClick(String categoryId, String categoryName) {
        Bundle params = new Bundle();
        if (categoryId != null) {
            params.putString(AnalyticsKeys.Param.CATEGORY_ID, categoryId);
        }
        if (categoryName != null) {
            params.putString(AnalyticsKeys.Param.CATEGORY_NAME, categoryName);
        }
        sendEvent(AnalyticsKeys.HOME_PAGE_CATEGORY_CLICK, params);
    }

    public void onClassLangTabClick(String categoryName, String tabsTitle) {
        Bundle params = new Bundle();
        if (categoryName != null) {
            params.putString(AnalyticsKeys.Param.CATEGORY_NAME, categoryName);
        }
        if (tabsTitle != null) {
            params.putString(AnalyticsKeys.Param.TABS_TITLE, tabsTitle);
        }
        if (getTags() != null) {
            params.putString(AnalyticsKeys.Param.TAG, getTags());
        }
        sendEvent(AnalyticsKeys.CLASS_LANG_TAB, params);
    }

    public void onClassItemClick(String categoryName, String tabsTitle, String classId, String className) {
        Bundle params = new Bundle();
        if (categoryName != null) {
            params.putString(AnalyticsKeys.Param.CATEGORY_NAME, categoryName);
        }
        if (tabsTitle != null) {
            params.putString(AnalyticsKeys.Param.TABS_TITLE, tabsTitle);
        }
        if (classId != null) {
            params.putString(AnalyticsKeys.Param.CLASS_ID, classId);
        }
        if (className != null) {
            params.putString(AnalyticsKeys.Param.CLASS_NAME, className);
        }
        if (getTags() != null) {
            params.putString(AnalyticsKeys.Param.TAG, getTags());
        }
        sendEvent(AnalyticsKeys.CLASS_CLICKED, params);
    }

    public void onSubjectClicked(String subjectId, String subjectName) {
        Bundle params = new Bundle();
        if (subjectId != null) {
            params.putString(AnalyticsKeys.Param.SUBJECT_ID, subjectId);
        }
        if (subjectName != null) {
            params.putString(AnalyticsKeys.Param.SUBJECT_NAME, subjectName);
        }
        if (getTags() != null) {
            params.putString(AnalyticsKeys.Param.TAG, getTags());
        }
        sendEvent(AnalyticsKeys.SUBJECT_CLICKED, params);
    }

    public void onSubjectSearchClicked(String search) {
        Bundle params = new Bundle();
        if (search != null) {
            params.putString(AnalyticsKeys.Param.SUBJECT_ID, search);
        }
        sendEvent(AnalyticsKeys.SUBJECT_SEARCH_CLICK, params);
    }

    public void onChapterClicked(String chapterId, String chapterName) {
        Bundle params = new Bundle();
        if (chapterId != null) {
            params.putString(AnalyticsKeys.Param.CHAPTER_ID, chapterId);
        }
        if (chapterName != null) {
            params.putString(AnalyticsKeys.Param.CHAPTER_NAME, chapterName);
        }
        if (getTags() != null) {
            params.putString(AnalyticsKeys.Param.TAG, getTags());
        }
        sendEvent(AnalyticsKeys.CHAPTER_CLICKED, params);
    }

    /**
     * @param buttonType : { Basic , Advance , Default ( Default as advance like anywhere in whole single item ) }
     */
    public void onChapterButtonClicked(@AnalyticsKeys.ViewType String buttonType) {
        Bundle params = new Bundle();
        if (buttonType != null) {
            params.putString(AnalyticsKeys.Param.TYPE, buttonType);
        }
        sendEvent(AnalyticsKeys.CHAPTER_BUTTON_CLICK, params);
    }

    public void onPDFDownloadClick(String pdfTitle) {
        Bundle params = new Bundle();
        if (pdfTitle != null) {
            params.putString(AnalyticsKeys.Param.PDF_TITLE, pdfTitle);
        }
        if (getTags() != null) {
            params.putString(AnalyticsKeys.Param.TAG, getTags());
        }
        sendEvent(AnalyticsKeys.PDF_DOWNLOAD_CLICK, params);
    }


    // push tags
    // in case of Home Tag clear bundle param
    // in case of lang clear bottom tags
    // in case of class clear subject tags
    // in case of subject tag override
    // structure -- Home -> lang -> class -> subject
    static Bundle bundle = new Bundle();
    public static void pushTags(String key , String title){
        bundle.putString(key , title);
    }



    public void onLogEventClick(String key , String title) {

    }
}
