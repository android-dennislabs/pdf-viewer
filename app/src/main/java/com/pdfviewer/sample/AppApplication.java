package com.pdfviewer.sample;

import android.app.Activity;
import android.net.Uri;
import android.text.TextUtils;

import com.config.config.ConfigManager;
import com.config.util.ConfigUtil;
import com.config.util.CryptoUtil;
import com.helper.application.BaseApplication;
import com.helper.callback.Response;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.template.PDFTemplateType;
import com.pdfviewer.util.PDFCallback;


public class AppApplication extends BaseApplication {

    private static AppApplication _instance;
    private ConfigManager configManager;

    public static AppApplication getInstance() {
        return _instance;
    }

    public ConfigManager getConfigManager() {
        if(configManager == null){
            configManager = ConfigManager.getInstance(this, ConfigUtil.getSecurityCode(this) , CryptoUtil.getUuidEncrypt(this), BuildConfig.DEBUG)
                    .setSecurityCodeEnc(true)
                    .setRequestTimeout(2)
                    .setEnableStatistics(false);
        }
        return configManager;
    }

    @Override
    public boolean isDebugMode() {
        return BuildConfig.DEBUG;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        PDFViewer.getInstance()
                .setTheme(this, PDFTemplateType.SELF_STUDY)
                .setFileProvider(this, getString(R.string.file_provider))
                .setDynamicShareListener(new PDFCallback.DynamicShare() {
                    @Override
                    public void onSharePDF(Activity activity, PDFModel pdfModel, int currentPage, Uri imagePath, Response.Progress progress) {
                        new AppDynamicShare(activity)
                                .addProgressListener(progress)
                                .sharePdf(pdfModel, currentPage, imagePath);
                    }
                })
                .init(this);
        PDFViewer.setDownloadDirectory(this,"PDFViewerApp");
        getConfigManager();
        loadConfigFromServer();
        CryptoUtil.initRemoteConfig(_instance, new CryptoUtil.onRemoteConfigLoad() {
            @Override
            public void onRemoteConfigLoad() {
                if ( configManager != null ){
                    configManager.setSecurityCodeEnc(CryptoUtil.getUuidEncrypt(_instance));
                    if ( !configManager.isConfigLoaded() ) {
                        loadConfigFromServer();
                    }
                }
            }
        });
    }

    @Override
    public void initLibs() {

    }

    public void loadConfigFromServer(){
        if ( !TextUtils.isEmpty( CryptoUtil.getUuidEncrypt(this) ) && !getConfigManager().isConfigLoaded() ) {
            configManager.loadConfig();
        }
    }

}
