package com.pdfviewer.sample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.reflect.TypeToken;
import com.helper.callback.Response;
import com.helper.util.BaseDynamicUrlCreator;
import com.helper.util.BaseUtil;
import com.helper.util.GsonParser;
import com.pdfviewer.model.PDFModel;
import com.pdfviewer.util.PDFDynamicShare;

import java.util.HashMap;


public class AppDynamicShare extends BaseDynamicUrlCreator {

    private static final String TAG = AppDynamicShare.class.getSimpleName();

    private Response.Progress progressListener;

    public AppDynamicShare(Context context) {
        super(context);
    }

    public static void open(Activity activity, Uri url, String extraData) {
        if (url != null && url.getQueryParameterNames() != null && url.toString().contains(ACTION_TYPE)) {
            if (url.getQueryParameter(ACTION_TYPE).equals(PDFDynamicShare.TYPE_PDF)) {
                PDFDynamicShare.open(activity, url, extraData);
            } else if (url.getQueryParameter(ACTION_TYPE).equals("others")) {

            }
        }
    }

    public void sharePdf(PDFModel pdfModel, int currentPage, Uri imagePath) {
        pdfModel.setOpenPagePosition(currentPage);
        pdfModel.setFilePath(null);

        HashMap<String, String> params = new HashMap<>();
        params.put(ACTION_TYPE, PDFDynamicShare.TYPE_PDF);
        String extraData = toJson(pdfModel, new TypeToken<PDFModel>() {
        });
        generate(params, extraData, new DynamicUrlCallback() {
            @Override
            public void onDynamicUrlGenerate(String url) {
                if (progressListener != null) {
                    progressListener.onStopProgressBar();
                }
                shareMe(url, imagePath);
            }

            @Override
            public void onError(Exception e) {
                if (progressListener != null) {
                    progressListener.onStopProgressBar();
                }
                Log.d(AppDynamicShare.class.getSimpleName(), "sharePdf:onError" + e.toString());
                shareMe(getPlayStoreLink(), imagePath);
            }
        });
    }

    @Override
    protected void onBuildDeepLink(@NonNull Uri deepLink, int minVersion, Context context, DynamicUrlCallback callback) {
        String uriPrefix = getDynamicUrl();
        if (!TextUtils.isEmpty(uriPrefix)) {
            DynamicLink.Builder builder = FirebaseDynamicLinks.getInstance()
                    .createDynamicLink()
                    .setLink(deepLink)
                    .setDomainUriPrefix(uriPrefix)
                    .setAndroidParameters(new DynamicLink.AndroidParameters.Builder()
                            .setMinimumVersion(minVersion)
                            .build());

            // Build the dynamic link
            builder.buildShortDynamicLink().addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                @Override
                public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                    if (task.isComplete() && task.isSuccessful() && task.getResult() != null
                            && task.getResult().getShortLink() != null) {
                        callback.onDynamicUrlGenerate(task.getResult().getShortLink().toString());
                    } else {
                        Log.d(TAG, "onBuildDeepLink:Error" + task.getException());
                        callback.onDynamicUrlGenerate(getPlayStoreLink());
                    }
                }
            });
        } else {
            callback.onError(new Exception("Invalid Dynamic Url"));
        }
    }

    private String getPlayStoreLink() {
        return "http://play.google.com/store/apps/details?id=" + context.getPackageName();
    }

    @Override
    protected void onDeepLinkIntentFilter(Activity activity) {
        if (activity != null && activity.getIntent() != null) {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(activity.getIntent())
                    .addOnSuccessListener(activity, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData linkData) {
                            if(resultCallBack != null) {
                                if (linkData != null && linkData.getLink() != null) {
                                    resultCallBack.onDynamicUrlResult(linkData.getLink()
                                            , EncryptData.decode(linkData.getLink().getQueryParameter(PARAM_EXTRA_DATA)));
                                } else {
                                    resultCallBack.onError(new Exception("Invalid Dynamic Url"));
                                }
                            }
                        }
                    })
                    .addOnFailureListener(activity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (resultCallBack != null) {
                                resultCallBack.onError(e);
                            }
                        }
                    });
        }
    }

    public AppDynamicShare addProgressListener(Response.Progress progressListener) {
        this.progressListener = progressListener;
        return this;
    }

    public void shareMe(String deepLink, Uri imageUri) {
        if (BaseUtil.isValidUrl(deepLink)) {
            Log.d(AppDynamicShare.class.getSimpleName(), deepLink);
            String text = "\nChick here to open : \n" + deepLink;

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            if(imageUri != null) {
                intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                intent.setType("image/*");
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(Intent.createChooser(intent, "Share With")
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}