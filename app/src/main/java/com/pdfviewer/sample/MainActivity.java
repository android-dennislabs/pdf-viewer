package com.pdfviewer.sample;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;

import com.config.config.ConfigConstant;
import com.config.statistics.BaseStatsActivity;
import com.config.statistics.model.StatisticsLevel;
import com.config.statistics.model.StatsLevelsModel;
import com.helper.util.BaseDynamicUrlCreator;
import com.helper.util.DayNightPreference;
import com.pdfviewer.PDFViewer;
import com.pdfviewer.stats.PDFStatsCreator;
import com.pdfviewer.stats.PDFStatsModel;
import com.pdfviewer.util.PDFCallback;

public class MainActivity extends BaseStatsActivity implements BaseDynamicUrlCreator.DynamicUrlResult{

    private static final String PDF_FILE_URL = "http://www.pdf995.com/samples/pdf.pdf";
    private static final String PDF_FILE_URL_1 = "https://www.selfstudys.com/api/v7/download-pdf/144711/" +
            "x76J9GCVeL5xjpPfmid5" +
            ".pdf";
    private static final String PDF_FILE_URL_2 = "https://www.selfstudys.com:8082/api/v7/download-pdf/175360/9DXYYAmLrBD0JWr2g4Vo.pdf";
    private static final String PDF_FILE_URL_3 = "https://www.selfstudys.com:8082/api/v7/download-pdf/475448/RYpa6xRucJKSw9IHH6Qj.pdf";
    //Large pdf file size
    //    private static final String PDF_FILE_URL_3 = "https://ars.els-cdn.com/content/image/1-s2.0-S0968089612008322-mmc1.pdf";//20 mb file size
    private static final String PDF_FILE_URL_4 = "https://ars.els-cdn.com/content/image/1-s2.0-S0308814616314601-mmc1.pdf";//40 mb file size
    private static final String PDF_FILE_URL_5 = "https://ars.els-cdn.com/content/image/1-s2.0-S1525001616328027-mmc2.pdf";//100mb file size
    private static final String PDF_FILE_NAME = "sample";
    private static final String PDF_FILE_NAME2 = "sample_2";
    private static final String PDF_TITLE = "Sample Pdf File";
    private static final String PDF_TITLE2 = "Sample Pdf File 2";
    private static final String PDF_PREFIX = "https://www.selfstudys.com:8082/api/v7/download-pdf/";
//Large pdf file size 6Mb
//    private static final String PDF_FILE_URL = "https://www.hq.nasa.gov/alsj/a410/AS08_CM.PDF";
//    private static final String PDF_FILE_NAME = "AS08_CM";
//    private static final String PDF_TITLE = "AS08_CM Pdf File";

//    private static final String PDF_FILE_URL2 = "https://www.emapsite.com/downloads/product_guides/Land-Form-Profile-user-guide.pdf";
//    private static final String PDF_FILE_NAME2 = "Land-Form-Profile-user-guide";
//    private static final String PDF_TITLE2 = "Land Form Profile user guide";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addStatistics(new StatisticsLevel(101, "pdf-viewer"));

        (findViewById(R.id.btn_open)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                PDFViewer.openPdfDownloadActivity(MainActivity.this, PDF_TITLE, PDF_FILE_NAME, PDF_FILE_URL, true);
                PDFViewer.openPdfDownloadActivity(MainActivity.this, 500, PDF_TITLE, PDF_FILE_NAME, PDF_PREFIX , PDF_FILE_URL_3, "Category 1", ConfigConstant.HOST_TRANSLATOR);
            }
        });
        (findViewById(R.id.btn_open2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                PDFViewer.openPdfDownloadActivity(MainActivity.this, PDF_TITLE, PDF_FILE_NAME, PDF_FILE_URL, true);
                PDFViewer.openPdfDownloadActivity(MainActivity.this, 501, PDF_TITLE2, PDF_FILE_NAME2, PDF_PREFIX , PDF_FILE_URL_2, "Category 2");
            }
        });
        (findViewById(R.id.btn_open1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDFViewer.openPdfDownloadActivity(MainActivity.this, 0, PDF_TITLE, PDF_FILE_NAME, PDF_FILE_URL, "Category 1", false, true, ConfigConstant.HOST_TRANSLATOR);
//                PDFViewer.openPdfDownloadActivity(MainActivity.this, 0, PDF_TITLE2, PDF_FILE_NAME2, PDF_FILE_URL2, "Category 1", false, true);
            }
        });
        (findViewById(R.id.btn_open_bookmark)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDFViewer.openPdfBookmarkActivity(v.getContext());
            }
        });

        (findViewById(R.id.btn_open_download)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDFViewer.openPdfDownloadedListActivity(v.getContext());
            }
        });

        SwitchCompat switchCompat = findViewById(R.id.switchCompat);
        if (DayNightPreference.isNightModeEnabled(this))
            switchCompat.setChecked(true);

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DayNightPreference.setNightMode(MainActivity.this, isChecked);
            }
        });

        PDFViewer.getInstance().addStatisticsCallbacks(new PDFCallback.StatsListener() {
            @Override
            public void onStatsUpdated() {
                publishStatsResult();
            }
        });

        onNewIntent(getIntent());
    }

    private void publishStatsResult() {
        PDFStatsCreator.getStatsJsonData(this, new PDFCallback.Statistics() {
            @Override
            public void onStatsUpdate(StatsLevelsModel<PDFStatsModel> response, String jsonData) {
                Toast.makeText(MainActivity.this, "" + jsonData, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            new AppDynamicShare(this)
                    .register(this);
        }
    }

    @Override
    public void onDynamicUrlResult(Uri url, String extraData) {
        Log.d("@PDFDynamic", "url: " + url.toString());
        Log.d("@PDFDynamic", "extraData: " + extraData);
        AppDynamicShare.open(this, url, extraData);
    }

    @Override
    public void onError(Exception e) {
        Log.d("@PDFDynamic", "onError: " + e.toString());
    }
}
