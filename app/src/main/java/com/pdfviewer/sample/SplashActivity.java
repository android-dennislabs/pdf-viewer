package com.pdfviewer.sample;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.app.AppCompatActivity;

import com.config.statistics.LastStats;
import com.helper.util.BaseUtil;
import com.login.LoginSdk;
import com.login.util.OnLoginCallback;


public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LastStats.clear(this);
        openLoginProcess();
    }

    private void goToHomeActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.putExtras(getIntent());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    finishAffinity();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    private void openLoginProcess() {
        if ( LoginSdk.getInstance().isUserLoginComplete() ) {
            goToHomeActivity();
        } else {
            LoginSdk.getInstance().setLoginCallback(new OnLoginCallback() {
                @Override
                public void onLoginSuccess() {
                    goToHomeActivity();
                }

                @Override
                public void onLoginSkipped() {
                    goToHomeActivity();
                }

                @Override
                public void onLoginFailure(Exception e) {
                    BaseUtil.showToast(SplashActivity.this, e.getMessage());
                    finishAffinity();
                }
            }).openLogin(this, true , false);
        }
    }
}
